# WebProjects
<!-- web projects -->

## - HTML & CSS

### [portfolio](https://saadmu7ammad.gitlab.io/template1_ElzeroWebSchool/#portfolio)
<!-- [Repo](https://gitlab.com/SaadMu7ammad/template1_ElzeroWebSchool)<br> -->
<!-- [website] -->


### [Kasper-template](https://saadmu7ammad.gitlab.io/template2_ElzeroWebSchool/)
<!-- [Repo](https://gitlab.com/SaadMu7ammad/template2_ElzeroWebSchool)<br> -->
<!-- ###[website] -->


### [Appie-template](https://saadmu7ammad.gitlab.io/project3-appie/)
<!-- [repo](https://gitlab.com/SaadMu7ammad/project3-appie)<br> -->
<!-- ###[website] -->


## - Javascript
-[20 Web Projects With Vanilla JS](https://gitlab.com/SaadMu7ammad/20-Web-Projects-With-Vanilla-JavaScript)

### [exchang-rate](https://saadmu7ammad.gitlab.io/exchange_rate/)


### [memory-card-games](https://saadmu7ammad.gitlab.io/memory_cards_game/)

### [Quran-player](https://saadmu7ammad.gitlab.io/quran_player/)

<!--### [project5](https://saadmu7ammad.gitlab.io/hangman_game/) -->

### [Sortable-list](https://saadmu7ammad.gitlab.io/sortableList/)

### [Quran-search](https://saadmu7ammad.gitlab.io/quran-search/)

### [Kanban Board](https://saadmu7ammad.gitlab.io/KanbanBoard/)


